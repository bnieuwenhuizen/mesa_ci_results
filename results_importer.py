# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *    Clayton Craft <clayton.a.craft@intel.com>
#  *    Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/

import argparse
import datetime
import fnmatch
import glob
import hashlib
import inotify.adapters
import json
import MySQLdb
import os
import shutil
import subprocess
import sys
import tempfile
import time
import xml.etree.cElementTree as et
import warnings


def q(s, escape=False):
    if not s:
        s = 'NULL'
    if escape and len(s):
        s = MySQLdb.escape_string(s)
    return '"' + s + '"'


class Tarball:
    def __init__(self, tarfile):
        self.tarfile = tarfile

    def __enter__(self):
        self.tmpdir = tempfile.mkdtemp()
        return self

    def __exit__(self, type, value, traceback):
        os.remove(self.tarfile)
        shutil.rmtree(self.tmpdir)

    def process(self):
        self.extract()
        build_info = self.get_build_info(self.tmpdir + '/build_info.json')
        if not build_info:
            raise RuntimeError("ERR: Unable to process build_info for tar "
                               "file: %s" % self.tarfile)
        return build_info, self.tmpdir

    def extract(self):
        try:
            subprocess.check_output(['tar', 'xf', self.tarfile,
                                     '-C', self.tmpdir])
        except subprocess.CalledProcessError:
            raise RuntimeError("WARN: Unable to extract tar file: %s"
                               % self.tarfile)

    def get_build_info(self, build_info_file):
        build_info = None
        try:
            with open(build_info_file, 'r') as f:
                build_info = json.load(f)
        except FileNotFoundError:
            print("WARN: build_info json not found: %s" % build_info_file)
        return build_info


class ResultsImporter:
    def __init__(self, watch_dir, sql_socket=None, sql_user='jenkins',
                 sql_host='localhost'):
        self.sql_pw = ''
        self.sql_user = sql_user
        self.sql_host = sql_host
        self.sql_socket = sql_socket
        self.watch_dir = watch_dir
        if "SQL_DATABASE_PW_FILE" in os.environ:
            sql_pw_file = os.environ["SQL_DATABASE_PW_FILE"]
            if os.path.exists(sql_pw_file):
                with open(sql_pw_file, 'r') as f:
                    self.sql_pw = f.read().rstrip()
        self.inot = inotify.adapters.Inotify()
        self.inot.add_watch(self.watch_dir)

    def poll(self):
        """ Blocks until a new tarball is found by inotify, then
        returns the path when one is found """
        for event in self.inot.event_gen(yield_nones=False):
            (_, type_names, path, filename) = event
            new_file = path + "/" + filename
            # Only proceed if the event was close after write
            if 'IN_CLOSE_WRITE' not in type_names:
                continue
            if not new_file or not os.path.exists(new_file):
                continue
            return new_file

    def consume(self, tarfile):
        """ Import data in given tarfile """
        with Tarball(tarfile) as tarball:
            try:
                self.build_info, self.resultdir = tarball.process()
            except RuntimeError:
                print("Skipping broken tar...")
                return
            print("Importing build: %s" % (self.build_info["name"]))
            revisions = ','.join([c + ':' + r for c, r in self.build_info['revisions'].items()])
            with ImportSql(host=self.sql_host, user=self.sql_user,
                           password=self.sql_pw, socket=self.sql_socket,
                           resultdir=self.resultdir,
                           start_time=self.build_info['start_time'],
                           end_time=self.build_info['end_time'],
                           url=self.build_info['url'],
                           revisions=revisions,
                           build_num=self.build_info["build"],
                           build_name=self.build_info["name"],
                           job=self.build_info["job"],
                           result_path=self.build_info["result_path"]) as importer:
                for component in self.build_info['components']:
                    importer.add_component(component)
        print("Done processing tarball: %s" % tarfile)


class Test:
    def __init__(self, etree):
        test_name = etree.attrib["name"]
        if "classname" in etree.attrib:
            test_name = ".".join([etree.attrib["classname"],
                                  etree.attrib["name"]])
        name_components = test_name.split(".")
        platform = name_components[-1]
        self.arch = platform[-3:]
        self.hardware = platform[:-3]
        self.name = ".".join(name_components[:-1])
        m = hashlib.md5()
        m.update(self.name)
        self._id = m.hexdigest()

        # status attribute stores the test result as produced by the suite
        if "status" in etree.attrib:
            self.status = etree.attrib["status"]
        else:
            self.status = "pass"
        if self.status == "crash":
            self.status = "fail"
        self.filtered_status = self.status

        # mesa ci overwrites fail/skip/error tags base on test status
        # in config files
        if etree.find("failure") is not None:
            self.filtered_status = "fail"
        elif etree.find("error") is not None:
            self.filtered_status = "fail"
        elif etree.find("skipped") is not None:
            self.filtered_status = "skip"
        else:
            self.filtered_status = "pass"
        self._stderr = ""
        err = etree.find("system-err")
        if err is not None and err.text is not None:
            self._stderr = err.text
        self._stdout = ""
        out = etree.find("system-out")
        if out is not None and out.text is not None:
            self._stdout = out.text

        self.time = 0.0
        if "time" in etree.attrib:
            self.time = float(etree.attrib["time"])

    def result_row(self, out_rows):
        out_rows.append((self._id, self.hardware, self.arch,
                         self.status, self.filtered_status, self.time,
                         self._stdout, self._stderr))


class TestTree:
    def __init__(self, groupname="root"):
        self._groupname = groupname
        m = hashlib.md5()
        m.update(groupname)
        self._group_id = m.hexdigest()

        # key is final test component (name.hwarch), value is Test object
        self._tests = {}
        self._subgroups = {}
        self._time = 0.0
        self._pass_count = 0
        self._filtered_pass_count = 0
        self._fail_count = 0

    def parse(self, xml):
        x = et.parse(xml)
        for test_tag in x.findall(".//testcase"):
            t = Test(test_tag)
            self.add_test(t)

    def add_test(self, test):
        test_path = test.name.split(".")
        subgroup = test_path[0]
        sub_test_path = test_path[1:]
        if subgroup not in self._subgroups:
            self._subgroups[subgroup] = TestTree(subgroup)
        self._subgroups[subgroup]._add_test(sub_test_path, test)
        self._time += test.time
        if test.filtered_status == "pass":
            self._pass_count += 1
        if test.filtered_status == "fail":
            self._fail_count += 1
        if test.filtered_status == "skip":
            self._filtered_pass_count += 1

    def _add_test(self, test_path, test):
        if test_path:
            subgroup = test_path[0]
            sub_test_path = test_path[1:]
            if subgroup not in self._subgroups:
                full_group = self._groupname + "." + subgroup
                self._subgroups[subgroup] = TestTree(full_group)
            self._subgroups[subgroup]._add_test(sub_test_path, test)
        else:
            self._tests[test.hardware + test.arch] = test
        self._time += test.time
        if test.filtered_status == "pass":
            self._pass_count += 1
        if test.filtered_status == "fail":
            self._fail_count += 1
        if test.filtered_status == "skip":
            self._filtered_pass_count += 1

    def group_rows(self, _out_rows=None):
        out_rows = []
        if _out_rows is not None:
            out_rows = _out_rows

        out_rows.append((self._group_id, self._pass_count, self._fail_count,
                         self._filtered_pass_count, self._time))
        for (_, subgroup) in self._subgroups.items():
            subgroup.group_rows(out_rows)

        if _out_rows is None:
            return out_rows

    def result_rows(self, _out_rows=None):
        out_rows = []
        if _out_rows is not None:
            out_rows = _out_rows
        for (_, t) in self._tests.items():
            t.result_row(out_rows)
        for (_, subgroup) in self._subgroups.items():
            subgroup.result_rows(out_rows)

        if _out_rows is None:
            return out_rows

    def test_known(self, cursor, test):
        cursor.execute("""select count(*) from test where test_id="{}" """.format(self._group_id))
        count = cursor.fetchone()[0]
        return (count > 0)

    def new_tests(self, cursor, _out_tests=None):
        out_tests = []
        if _out_tests is not None:
            out_tests = _out_tests

        # query db to see if any test ids must be added
        current_count = len(out_tests)
        for (_, subgroup) in self._subgroups.items():
            subgroup.new_tests(cursor, out_tests)

        if self._subgroups and len(out_tests) == current_count:
            # adding any of the subgroups would have also added this
            # group.
            return
        if not self.test_known(cursor, self._group_id):
            out_tests.append(self._groupname)
        if _out_tests is None:
            return out_tests

    def test_rows(self, tests):
        out_rows = []
        for t in tests:
            m = hashlib.md5()
            m.update(t)
            out_rows.append((m.hexdigest(), t))
        return out_rows

    def parent_rows(self, tests, _out_rows=None):
        out_rows = []
        if _out_rows is not None:
            out_rows = _out_rows
        for t in tests:
            m = hashlib.md5()
            m.update(t)
            test_id = m.hexdigest()
            groups = t.split(".")
            if len(groups) > 1:
                parent = ".".join(groups[:-1])
            elif t == "root":
                continue
            else:
                parent = "root"
            m = hashlib.md5()
            m.update(parent)
            parent_id = m.hexdigest()
            out_rows.append((test_id, parent_id))
        if _out_rows is None:
            return out_rows


class ImportSql:
    def __init__(self, resultdir, start_time, end_time, url, revisions,
                 build_name, build_num, job, result_path, host='localhost',
                 user='jenkins', password=None,
                 socket=None):
        warnings.filterwarnings('ignore', category=MySQLdb.Warning)
        self.host = host
        self.user = user
        self.socket = socket
        self.password = password
        self.resultdir = resultdir
        self.build_num = build_num
        self.build_name = build_name
        self.job = job
        self.url = url
        self.result_path = result_path
        self.revisions = revisions
        if not start_time:
            start_time = 515462400
        self.build_start_time = datetime.datetime.fromtimestamp(
            int(start_time)).strftime('%Y-%m-%d %H:%M:%S')
        self.build_end_time = datetime.datetime.fromtimestamp(
            int(end_time)).strftime('%Y-%m-%d %H:%M:%S')

    def __enter__(self):
        self.connect()
        self.create_db()
        self.remove_duplicate_build()
        return self

    def __exit__(self, type, value, traceback):
        self.disconnect()

    def connect(self):
        if self.socket:
            self.db = MySQLdb.connect(user=self.user,
                                      unix_socket=self.socket,
                                      passwd=self.password)
        else:
            self.db = MySQLdb.connect(host=self.host, user=self.user,
                                      passwd=self.password)
        self.cur = self.db.cursor()

    def create_db(self):
        # '-' and '.' not allowed in database names
        job = self.job.replace("-", "_")
        job = self.job.replace(".", "_")
        self.cur.execute("create database if not exists {}".format(job))
        self.cur.execute("use {}".format(job))
        self.cur.execute("SET @@session.unique_checks = 0")
        self.cur.execute("SET @@session.foreign_key_checks = 0")
        self.db.commit()

        # build table
        self.cur.execute("create table if not exists build (" 
                         "build_id int not null,  "
                         "build_name char(100), "
                         "pass_count int, "
                         "fail_count int, "
                         "start_time datetime, "
                         "end_time datetime, "
                         "url char(128) not null, "
                         "revisions text(512), "
                         "filtered_pass_count int,"
                         "result_path varchar(1024), "
                         "primary key(build_id));")
        # test table
        self.cur.execute("create table if not exists test ("
                         "test_id char(32) not null, "
                         "test_name text(1024) not NULL, "
                         "primary key(test_id))")
        # parent table
        self.cur.execute("create table if not exists parent ("
                         "test_id char(32) not null, "
                         "parent_id char(32) not null, "
                         "primary key(test_id), "
                         "index parent_id(parent_id))")
        # result table
        self.cur.execute("create table if not exists result ("
                         "result_id int not null AUTO_INCREMENT,  "
                         "test_id char(32) not null, "
                         "arch enum('m32', 'm64') not null, "
                         "hardware char(32), "
                         "build_id int not null, "
                         "status enum('pass', 'fail', 'skip') not null, "
                         "filtered_status enum('pass', 'fail', 'skip') not null, "
                         "time decimal(32,6), "
                         "stdout text, "
                         "stderr text, "
                         "primary key(result_id), "
                         "index build_id(build_id), "
                         "index test_id(test_id), "
                         "index filtered_status(filtered_status)"
                         ")")
        # group_ table
        self.cur.execute("create table if not exists group_ ("
                         "build_id int not null,  "
                         "test_id char(32) not null, "
                         "pass_count int not null, "
                         "fail_count int not null, "
                         "filtered_pass_count int not null, "
                         "time decimal(32,6) not null, "
                         "primary key(build_id, test_id))")
        # artifact table
        self.cur.execute("create table if not exists artifact ("
                         "artifact_id int not null AUTO_INCREMENT, "
                         "type char(64) not null, "
                         "filename char(120) not null, "
                         "data longtext not null, "
                         "component_id int not null,"
                         "index component_id(component_id),"
                         "primary key(artifact_id))")
        # component table
        self.cur.execute("create table if not exists component ("
                         "component_id int not null AUTO_INCREMENT, "
                         "arch enum('m32', 'm64') not null, "
                         "status enum('success', 'failure', 'unstable', 'aborted') not null, "
                         "component_name char(100), "
                         "shard char(32), "
                         "machine char(64), "
                         "start_time datetime, "
                         "end_time datetime, "
                         "build int not null, "
                         "build_id int not null, "
                         "primary key(component_id))")
        self.db.commit()

    def add_component(self, component):
        start_time = datetime.datetime.fromtimestamp(
            int(component['start_time'])).strftime('%Y-%m-%d %H:%M:%S')
        # note: end_time may be null if the component failed
        if component['end_time']:
            end_time = datetime.datetime.fromtimestamp(
                int(component['end_time'])).strftime('%Y-%m-%d %H:%M:%S')
        else:
            end_time = start_time
        if 'status' not in component.keys() or not component['status']:
            status = 'aborted'
        else:
            status = component['status']
        # These values are allowed to be null
        machine = component['machine'] if 'machine' in component.keys() else ''
        shard = component['shard'] if 'shard' in component.keys() else ''

        self.cur.execute("""insert into component (arch, component_name, """
                         """shard, machine, start_time, end_time, """
                         """build_id, build, status) values """
                         """({}, {}, {}, {}, {}, {}, {}, {}, {})"""
                         """""".format(q(component['arch']),
                                       q(component['name']),
                                       q(shard),
                                       q(machine), q(start_time),
                                       q(end_time), self.build_num,
                                       component['build'],
                                       q(status)))
        self.db.commit()
        self.cur.execute("select last_insert_id()")
        component_id = self.cur.fetchone()[0]
        if 'artifacts' in component.keys():
            for artifact in component['artifacts']:
                if artifact['type'] == 'junit':
                    self.import_xml(self.resultdir + "/" + artifact['file'])
                else:
                    # note: currently only support importing text (not binary)
                    with open(self.resultdir + '/'
                              + artifact['file'], 'r') as f:
                        data = f.read()
                        data = q(data, escape=True)
                    self.cur.execute("""insert into artifact (type, """
                                     """filename, component_id, data) """
                                     """values ({}, {}, {}, {})"""
                                     """""".format(q(artifact['type']),
                                                   q(artifact['file']),
                                                   component_id, data))
                    self.db.commit()

    def remove_duplicate_build(self):
        # Clean out results table
        self.cur.execute("""select count(*) from result where build_id="{}" """.format(self.build_num))
        count = int(self.cur.fetchone()[0])
        while count > 0:
            print "deleting results from previous matching build: " + str(count)
            self.cur.execute("delete quick from result where build_id={} limit 100000".format(self.build_num))
            self.db.commit()
            self.cur.execute("""select count(*) from result where build_id="{}" """.format(self.build_num))
            count = int(self.cur.fetchone()[0])
        # Clean out group_ table
        self.cur.execute("""select count(*) from group_ where build_id="{}" """.format(self.build_num))
        count = int(self.cur.fetchone()[0])
        while count > 0:
            print "deleting group stats from previous matching build: " + str(count)
            self.cur.execute("delete quick from group_ where build_id={} limit 100000".format(self.build_num))
            self.db.commit()
            self.cur.execute("""select count(*) from group_ where build_id="{}" """.format(self.build_num))
            count = int(self.cur.fetchone()[0])
        # Clean out components table
        self.cur.execute("""select component_id from component where build_id="{}" """.format(self.build_num))
        for component_id in self.cur.fetchall():
            # Clean out artifacts table
            self.cur.execute("delete quick from artifact where "
                             "component_id=%s", [component_id])
            self.db.commit()
        self.cur.execute("""select count(*) from component where build_id="{}" """.format(self.build_num))
        count = int(self.cur.fetchone()[0])
        print "deleting components from previous matching build: " + str(count)
        self.cur.execute("delete quick from component where build_id={} limit 100000".format(self.build_num))
        self.db.commit()

    def import_xml(self, xml_file):
        print("importing file: %s" % xml_file)
        tt = TestTree()
        tt.parse(xml_file)

        new_tests = tt.new_tests(self.cur)
        if new_tests:
            print "found {} new tests".format(str(len(new_tests)))
            test_rows = tt.test_rows(new_tests)
            values = ["({}, {})".format(q(id), q(name)) for (id, name) in test_rows]
            self.cur.execute("""insert ignore into test (test_id, test_name) values """ +
                             ", ".join(values))

            parent_rows = tt.parent_rows(new_tests)
            values = ["({}, {})".format(q(id), q(parent)) for (id, parent) in parent_rows]
            self.cur.execute("insert ignore into parent "
                             "(test_id, parent_id) values " +
                             ", ".join(values))
            self.db.commit()

        print "updating group statistics"
        groups = tt.group_rows()
        values = ["({}, {}, {}, {}, {}, {})".format(self.build_num,
                                                    q(test_id),
                                                    pass_count,
                                                    fail_count,
                                                    filtered_pass_count,
                                                    time)
                  for (test_id, pass_count, fail_count, filtered_pass_count, time) in groups]
        self.cur.execute("insert into group_ ("
                         "build_id, test_id, pass_count, "
                         "fail_count, filtered_pass_count, time) "
                         "values " +
                         ", ".join(values) +
                         " on duplicate key update "
                         "pass_count = pass_count + values(pass_count), "
                         "fail_count = fail_count + values(fail_count), "
                         "filtered_pass_count = filtered_pass_count + "
                         "values(filtered_pass_count), "
                         "time = time")
        self.db.commit()

        print "updating test results"
        results = tt.result_rows()
        values = ["({}, {}, {}, {}, {}, {}, {}, {}, {})".format(q(test_id), q(hardware), q(arch), self.build_num,
                                                                q(status), q(filtered_status), time,
                                                                q(stdout, True), q(stderr, True))
                  for (test_id, hardware, arch, status, filtered_status, time, stdout, stderr) in results]
        self.cur.execute("insert ignore into result ("
                         "test_id, hardware, arch, build_id, status, "
                         "filtered_status, time, stdout, stderr) values " +
                         ", ".join(values))
        self.db.commit()

        print "updating build statistics"
        m = hashlib.md5()
        m.update("root")
        self.cur.execute("select pass_count, filtered_pass_count, fail_count "
                         "from group_ where build_id={} and test_id={}".format(self.build_num,
                                                                               q(m.hexdigest())))
        (pass_count, filtered_pass_count, fail_count) = self.cur.fetchone()

        self.cur.execute("""replace into build(build_id, build_name, """
                         """pass_count, filtered_pass_count, fail_count, """
                         """start_time, end_time, url, revisions, """
                         """result_path) """
                         """values({}, {}, {}, {}, {}, {}, {}, {}, {}, {})""".format(self.build_num, q(self.build_name),
                                                                                     pass_count, filtered_pass_count, fail_count,
                                                                                     q(self.build_start_time), q(self.build_end_time),
                                                                                     q(self.url), q(self.revisions), q(self.result_path)))
        self.db.commit()
        print "import done"

    def disconnect(self):
        self.db.close()


def main():
    results_path = '/tmp/mesa_ci_results'
    if not os.path.exists(results_path):
        os.mkdir(results_path)

    sql_host = 'localhost'
    if "SQL_DATABASE_HOST" in os.environ:
        sql_host = os.environ["SQL_DATABASE_HOST"]

    sql_user = 'jenkins'
    if "SQL_DATABASE_USER" in os.environ:
        sql_user = os.environ["SQL_DATABASE_USER"]

    sql_socket = None
    if "SQL_DATABASE_SOCK" in os.environ:
        sql_socket = os.environ["SQL_DATABASE_SOCK"]

    results_importer = ResultsImporter(results_path, sql_host=sql_host,
                                       sql_user=sql_user,
                                       sql_socket=sql_socket)
    # consume any existing tar files
    for tarfile in glob.glob(results_path + "/*.xz"):
        results_importer.consume(tarfile)
    while True:
        tarfile = results_importer.poll()
        results_importer.consume(tarfile)


if __name__ == "__main__":
    main()
